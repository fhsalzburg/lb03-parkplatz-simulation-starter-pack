package at.fhsalzburg.its.nos.parksim;

public class GateWorker extends Thread {
    private final Gate gate;
    private final ParkingArea parkingArea;
    private final CarFactory factory;

    public GateWorker(Gate gate, ParkingArea area, CarFactory factory)
    {
        this.gate = gate;
        this.parkingArea = area;
        this.factory = factory;
    }
    
    @Override
    public void run()
    {
        while (gate.isOpen())
        {
            try
            {
                Thread.sleep(2000);
                double insert = Math.random();
                // for 70% cases we want to drive in
                // in the other 30% cars should drive out
                if (insert < 0.7f)
                {
                    Car car = factory.createNew();
                    gate.driveIn(car);
                }
                else
                {
                	//search for occupied place
                    ParkingPlace place = parkingArea.findOccupiedPlace();
                    
                    if (place != null) {
                    	
                    	//get placed car
                    	Car car = place.getPlacedCar();
                    	if (car != null)
                        {
                            gate.driveOut(car);
                        }
                	}
                }
            }
            catch (InterruptedException e)
            {
            	// TODO: handle the interrupt accordingly, shutdown gate
            }
            catch (NoFreePlaceException e)
            {
            	// TODO: print a message the place is full
            }
        }
    }
}
