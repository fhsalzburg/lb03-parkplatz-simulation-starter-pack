package at.fhsalzburg.its.nos.parksim;

public interface Gate {
	/**
	 * Handles the attempt of a car to drive into the parking 
	 * information system. 
	 * @param car The car aiming to drive in
	 * 
	 * @throws NoFreePlaceException In case the parking place is full an 
	 * driving in is not permitted.
	 */
	public void driveIn(Car car) throws NoFreePlaceException;
	/**
	 * Handles the quitting of a car from the parking place
	 * @param car The car currently leaving
	 */
	public void driveOut(Car car);

	public boolean isOpen();
	public void setGateOpen(boolean isOpen);
}
