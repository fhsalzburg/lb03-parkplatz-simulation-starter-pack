/**
 * 
 */
package at.fhsalzburg.its.nos.parksim;


/**
 * @author eduard
 *
 */
public abstract class Car {
	protected final String plateNumber;
	protected final HandicappedStatus hadicappedStatus;
	
	public Car(HandicappedStatus hadicapedStatus, String licensePlateNumber) {
		plateNumber = licensePlateNumber;
		this.hadicappedStatus = hadicapedStatus;
	}
	
	public String getLicensePlateNumber() {
		return plateNumber;
	}
	public HandicappedStatus handicappedStatus() {
		return hadicappedStatus;
	}
}