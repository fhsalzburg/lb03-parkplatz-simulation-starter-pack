/**
 * 
 */
package at.fhsalzburg.its.nos.parksim;

import java.util.List;

/**
 * @author eduard
 *
 */
public interface ParkingArea {
	/** 
	 * Method handling the placement of a car 
	 * on any of the parking place(s). 
	 * @param car The car to place in the parkingArea
	 * @return The list of used parking places
	 */
	public List<ParkingPlace> placeCar(Car car);

	/**
	 * Method handling the removement of a car
	 * from the occupied parking place(s)
	 * @param car The car to remove
	 * @return The list of no longer occupied places
	 */
	public List<ParkingPlace> removeCar(Car car);

	/**
	 * Method verifying whether there is free space
	 * for a car 
	 * @return true if there is free space, false otherwise
	 */
	public boolean hasFreeParkingPlace(Car car);

	/**
	 * Chooses an occupied ParkingPlace
	 * @return ParkingPlace occupied
     */
	public ParkingPlace findOccupiedPlace();
}
