package at.fhsalzburg.its.nos.parksim;

public interface CarFactory {
	
	/**
	 * Factory method creating a new filled instance of a car of random type
	 * @return The new instance
	 */
	public Car createNew();
	
	/**
	 * Factory method creating a new filled instance of a car of specific type
	 * @return The new instance
	 */
	public Car createNew(HandicappedStatus status, String licensePlateNumber);
}