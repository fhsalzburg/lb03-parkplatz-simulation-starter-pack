package at.fhsalzburg.its.nos.parksim;

public interface ParkingPlace {
	public boolean reservedForHandicappedCars();
	public HandicappedStatus getHandicappedStatus();
	public Car getPlacedCar();
	public void placeCar(Car car);
}