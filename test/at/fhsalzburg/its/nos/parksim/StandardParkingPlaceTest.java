package at.fhsalzburg.its.nos.parksim;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;
import org.junit.Test;

import at.fhsalzburg.its.nos.parksim.Car;
import at.fhsalzburg.its.nos.parksim.HandicappedStatus;
import at.fhsalzburg.its.nos.parksim.StandardParkingPlace;

public class StandardParkingPlaceTest {

	@Test
	public void testHandicapedStatus() {
		StandardParkingPlace ppNormal = new StandardParkingPlace(HandicappedStatus.NORMAL);
		StandardParkingPlace ppHandicapped = new StandardParkingPlace(HandicappedStatus.HANDICAP);
		
		assertEquals(HandicappedStatus.NORMAL, ppNormal.getHandicappedStatus());
		assertFalse(ppNormal.reservedForHandicappedCars());
		
		assertEquals(HandicappedStatus.HANDICAP, ppHandicapped.getHandicappedStatus());
		assertTrue(ppHandicapped.reservedForHandicappedCars());
	}
	
	@Test
	public void testCarPlacement() {
		StandardParkingPlace pp = new StandardParkingPlace(HandicappedStatus.NORMAL);
		Car car = mock(Car.class);
		
		pp.placeCar(car);
		
		assertEquals(car, pp.getPlacedCar());
	}
}
