package at.fhsalzburg.its.nos.parksim;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import org.junit.Test;

import at.fhsalzburg.its.nos.parksim.Car;
import at.fhsalzburg.its.nos.parksim.HandicappedStatus;

public class CarTest {

	@Test
	public void testAbstractCarContainer() {
		String licencePlate = "HA-340 FH";
		
		// instantiate anonymous class
		Car car = new Car(HandicappedStatus.NORMAL, licencePlate) {};
		
		//Simple Assertion Test
		assertEquals(HandicappedStatus.NORMAL, car.handicappedStatus());
		assertEquals(licencePlate, car.getLicensePlateNumber());
	}
	
	@Test
	public void testMockitoLibAddedCorrectly() {
		// instantiate anonymous class
		Car car = mock(Car.class);
		when(car.handicappedStatus()).thenReturn(HandicappedStatus.NORMAL);
		
		assertEquals(HandicappedStatus.NORMAL, car.handicappedStatus());
	}
}