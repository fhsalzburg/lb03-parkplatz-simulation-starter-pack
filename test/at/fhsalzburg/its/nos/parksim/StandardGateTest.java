package at.fhsalzburg.its.nos.parksim;

import static org.mockito.Mockito.*;


import static org.junit.Assert.*;

import org.junit.Test;

import at.fhsalzburg.its.nos.parksim.Car;
import at.fhsalzburg.its.nos.parksim.NoFreePlaceException;
import at.fhsalzburg.its.nos.parksim.ParkingArea;
import at.fhsalzburg.its.nos.parksim.StandardGate;

public class StandardGateTest {

	@Test
	public void testDriveIn() {
		ParkingArea area = mock(ParkingArea.class);
		Car car = mock(Car.class);		
		
		StandardGate sg = new StandardGate(area);
		try {
			sg.driveIn(car);
		} catch (NoFreePlaceException e) {
			fail(e.getMessage());
		}
		
		verify(area).placeCar(car);
	}
	
	@Test(expected=NoFreePlaceException.class)
	public void testAreaFull() throws NoFreePlaceException {
		ParkingArea area = mock(ParkingArea.class);
		Car car = mock(Car.class);		
		
		when(area.hasFreeParkingPlace(car)).thenReturn(false);
		
		StandardGate sg = new StandardGate(area);
		
		sg.driveIn(car);
	}
}